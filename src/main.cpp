#include "Arduino.h"

#include <Servo.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32MultiArray.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>

#define fmap(x, smin, smax, emin, emax) ((((x - smin) * (emax - emin)) / (smax - smin)) + emin)
#define fabs(x) (x > 0.0f ? x : x * -1.0f)

int motor_count;
int* motor_pins;
int* motor_config;
Servo* motors;
int* motor_pwm;
long time_last_motor_command = 0;
long time_last_enable_motors = 0;

int pressure_pin;
int kill_pin_ctrl;
int kill_pin_sense;
int debounce_count;

int running = false;
int externalPressure = 0;

ros::NodeHandle  nh;
Adafruit_BMP085 bmp;

void cmd_motor_callback(const std_msgs::Float32MultiArray& cmd_msg){
  time_last_motor_command = millis();

  bool enableConfig = fabs(cmd_msg.data[0]) > 0.01f;

  for(size_t i = 0; i < motor_count && i < cmd_msg.data_length - 1; ++i){
    int pinIndex = enableConfig ? abs(motor_config[i]) : i + 1;
    bool forward = enableConfig ? motor_config[i] > 0 : true;

    int power = min(max(static_cast<int>(fmap(
      cmd_msg.data[pinIndex],
      (forward ? -1.0f : 1.0f),
      (forward ? 1.0f : -1.0f),
      1100.0f,
      1900.0f
    )), 1100), 1900);

    motor_pwm[i] = power;
  }
}

void enable_motor_callback(const std_msgs::Float32 cmd_msg){
  time_last_enable_motors = millis();
  bool power_on_motors = cmd_msg.data;
  if(power_on_motors){
    digitalWrite(kill_pin_ctrl, HIGH);
  }else{
    digitalWrite(kill_pin_ctrl, LOW);
  }
}

std_msgs::Float32 pressure_msg;
std_msgs::Bool kill_msg;

ros::Subscriber<std_msgs::Float32MultiArray> cmd_motor_sub("cmd_motors", cmd_motor_callback);
ros::Subscriber<std_msgs::Float32> cmd_motor_sub("enable_motors", enable_motor_callback);
ros::Publisher pressure_pub("pressure", &pressure_msg);
ros::Publisher kill_pub("kill_switch", &kill_msg);

void getParameters(){
  nh.getParam("/arduino/motor_count", &motor_count);
  motor_pins = new int[motor_count];
  nh.getParam("/arduino/motor_pins", motor_pins, motor_count);
  motor_config = new int[motor_count];
  nh.getParam("/arduino/motor_config", motor_config, motor_count);
  motors = new Servo[motor_count];
  motor_pwm = new int[motor_count];
  for(int i = 0; i < motor_count; ++i){
    char result[10];
    sprintf(result, "S: %d", motor_pins[i]);
    nh.loginfo(result);

    motors[i].attach(motor_pins[i]);
    motors[i].writeMicroseconds(1500);
    motor_pwm[i] = 1500;
  }

  nh.getParam("/arduino/pressure_pin", &pressure_pin);
  nh.getParam("/arduino/external_pressure", &externalPressure);
  nh.getParam("/arduino/kill_pin_ctrl", &kill_pin_ctrl);
  nh.getParam("/arduino/kill_pin_sense", &kill_pin_sense);
  pinMode(kill_pin_ctrl, OUTPUT);
  digitalWrite(kill_pin_ctrl, LOW); // keep motors off
  pinMode(kill_pin_sense, INPUT);
  nh.loginfo("Setup Kill Switch");
  nh.getParam("/arduino/debounce_count", &debounce_count);
}

void setup(){
  nh.getHardware()->setBaud(115200);
  nh.initNode();
  nh.subscribe(cmd_motor_sub);
  nh.advertise(pressure_pub);
  nh.advertise(kill_pub);
  while(!nh.connected()){
    nh.spinOnce();
  }
  delay(10);
  nh.loginfo("Arduino Connected");
  getParameters();
  if (!externalPressure&&!bmp.begin()) {
	  nh.logerror("Could not find a valid BMP085 sensor, check wiring!");
  }
  nh.spinOnce();
}


void readPressureSensor() {
  static unsigned int nextRun = 0;
  if(millis() > nextRun) {
    if(externalPressure) {
      pressure_msg.data = analogRead(pressure_pin);
    } else {
      pressure_msg.data = bmp.readPressure();
    }
    pressure_pub.publish(&pressure_msg);
    nextRun = millis()+50;
  }
}


void readKillSwitch() {
  static int readCount = 0;
  bool killSwitch = !digitalRead(kill_pin_sense);
  if(killSwitch == running) {//D22 - 5V
    readCount ++;
    if(readCount > debounce_count) {
      kill_msg.data = killSwitch;
      kill_pub.publish(&kill_msg);
      running = !killSwitch;
      if(!running) {
        for(size_t i = 0; i < motor_count; i++) {
          motors[i].writeMicroseconds(1500);
        }
      }
    }
  } else {
    readCount = 0;
  }
}

void applyMotorPWM(){
  long now = millis();
  // lock motors if we haven't got a command to move them in the past 0.5 seconds
  if(now > time_last_motor_command + 500){
    for(size_t i = 0; i < motor_count; ++i){
      motor_pwm[i] = 1500;
    }
  }
  // apply motor values
  for(size_t i = 0; i < motor_count; ++i){
    motors[i].writeMicroseconds(motor_pwm[i]);
  }
}

void watchdogEnableMotors(){
  long now = millis();
  if(now > time_last_enable_motors + 5000){
    time_last_enable_motors = now;
    digitalWrite(kill_pin_ctrl, LOW);
  }
}

void loop(){
  readPressureSensor();
  readKillSwitch();
  watchdogEnableMotors();
  applyMotorPWM();
  nh.spinOnce();
  delay(1);
}
